#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/aruco.hpp>

#include <iostream>
#include <stdio.h>

#include "std_msgs/String.h"

#include <sstream>

#define USB_CAM
// #define Jetson_CAM 

using namespace cv;
using namespace std;

std::string gstreamer_pipeline(int capture_width, int capture_height, int display_width, int display_height, int framerate, int flip_method)
{
// /*
    return "nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(capture_width) + ", height=(int)" +
           std::to_string(capture_height) + ", format=(string)NV12, framerate=(fraction)" + std::to_string(framerate) +
           "/1 ! nvvidconv flip-method=" + std::to_string(flip_method) + " ! video/x-raw, width=(int)" + std::to_string(display_width) + ", height=(int)" +
           std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
// */
}

int main(int argc, char **argv)
{

    std::cout << "OpenCV version : " << CV_VERSION << endl;
    std::cout << "Major version : " << CV_MAJOR_VERSION << endl;
    std::cout << "Minor version : " << CV_MINOR_VERSION << endl;
    std::cout << "Subminor version : " << CV_SUBMINOR_VERSION << endl;


    ros::init(argc, argv, "talker");
    ros::NodeHandle n;

#ifdef USB_CAM
    //web_cam****************************
    // cv::VideoCapture capture(0);
    cv::VideoCapture cap(2);
    if (!cap.open(0))
    {
        std::cout << "Failed to open camera." << std::endl;
        return (-1);
    }
    // cv::namedWindow("CSI Camera", cv::WINDOW_AUTOSIZE);
    cv::Mat img;
    //web_cam****************************
#endif

#ifdef Jetson_CAM
    // jetson_cam****************************
    int capture_width = 1280;
    int capture_height = 720;
    int display_width = 1280;
    int display_height = 720;
    int framerate = 30;
    int flip_method = 0;

    std::string pipeline = gstreamer_pipeline(capture_width,
                                              capture_height,
                                              display_width,
                                              display_height,
                                              framerate,
                                              flip_method);
    std::cout << "Using pipeline: \n\t" << pipeline << "\n";

    cv::VideoCapture cap(pipeline, cv::CAP_GSTREAMER);
    if (!cap.isOpened())
    {
        std::cout << "Failed to open camera." << std::endl;
        return (-1);
    }

    cv::namedWindow("CSI Camera", cv::WINDOW_AUTOSIZE);
    cv::Mat img;
    // jetson_cam****************************
#endif

    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    image_transport::ImageTransport it_(n);
    image_transport::Publisher image_pub_ = it_.advertise("/traj_output", 1);
    cv_bridge::CvImagePtr cv_ptr(new cv_bridge::CvImage);
    ros::Rate loop_rate(10);

    int count = 0;
    while (ros::ok())
    {

        cap >> img;

        ros::Time time = ros::Time::now();
        cv_ptr->encoding = "bgr8";
        cv_ptr->header.stamp = time;
        cv_ptr->header.frame_id = "/traj_output";
        cv_ptr->image = img;
        image_pub_.publish(cv_ptr->toImageMsg());

        cv::imshow("CSI Camera", img);

        int keycode = cv::waitKey(30) & 0xff;
        if (keycode == 27)
            break;

        // return 0;

        std_msgs::String msg;
        std::stringstream ss;
        ss << "hello world " << count;
        msg.data = ss.str();
        // ROS_INFO("%s", msg.data.c_str());
        chatter_pub.publish(msg);
        ros::spinOnce();

        ++count;
    }

    cap.release();
    cv::destroyAllWindows();
    return 0;
}
